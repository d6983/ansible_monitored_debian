#!/usr/bin/env bash

KEY=id_deploy

if [[ ! -f $KEY ]]; then
ssh-keygen -t ed25519 -C "gitlab_deploy_key" -N "" -f $KEY
fi
